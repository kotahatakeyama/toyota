$(document).ready(function() {
    $("a").bind("focus", function() {
        if (this.blur) this.blur();
    });
});

$(document).ready(function() {

    // 動画高さ
    var mv = $(".movie iframe");
    var mvWid = mv.width();
    var mvHeight = mvWid * 0.526315;
    $(mv).css("height", mvHeight);

    // spmenu
    $(".menuBtn img").click(function() {
        var headHeight = $("header").height();
        $(".menuBtn").toggle(
            function() {
                $(".menuBtn").html('<img src="images/img_menu_close.png" alt="MENU">');
                $(".spNav").fadeToggle(300);
                $(".spNav").css("padding-top", headHeight);
            },
            function() {
                $(".menuBtn").html('<img src="images/img_menu_open.png" alt="MENU">');
                $(".spNav").fadeToggle(300);
            }
        );
        $(".spNav").click(function() {
            var headHeight = $("header").height();
            $(".menuBtn").html('<img src="images/img_menu_open.png" alt="MENU">');
            $(".spNav").fadeToggle(300);
        });
    });


    // インフォメーションを中央寄せ
    $(window).on('load resize', function() {
        var wid = $(window).width();
        if (wid > 768) {
            var f_height = $('.info .col:first-of-type').height();
            var l_height = $('.info .col:last-of-type').height();
            var child = $(".infoArea").children('dl');
            var dlLength = child.length;
            var areaHight = 20 * dlLength;
            if (l_height <= f_height) {
                $('.info .col:last-of-type').css("height", f_height);
                $('.infoArea').css("height", areaHight);
            }
        }
    });

    //動画モーダルプラグイン
    $('.popup-iframe').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 200,
        preloader: false
    });

    //モーダル
    $('a[rel*=leanModal]').leanModal({
        top: 0,
        closeButton: ".closeBtn"
    });

});

$(window).load(function() {
    $('.bxslider1').bxSlider({
        pagerCustom: '.bx-pager1',
        infiniteLoop: true,
        controls: true,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider2').bxSlider({
        pagerCustom: '.bx-pager2',
        infiniteLoop: true,
        controls: true,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider3').bxSlider({
        pagerCustom: '.bx-pager3',
        infiniteLoop: true,
        controls: true,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider4').bxSlider({
        pagerCustom: '.bx-pager4',
        infiniteLoop: true,
        controls: true,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider5').bxSlider({
        pagerCustom: '.bx-pager5',
        infiniteLoop: true,
        controls: true,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider6').bxSlider({
        pagerCustom: '.bx-pager6',
        infiniteLoop: true,
        controls: true,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider7').bxSlider({
        pagerCustom: '.bx-pager7',
        infiniteLoop: true,
        controls: true,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider8').bxSlider({
        pagerCustom: '.bx-pager8',
        infiniteLoop: true,
        controls: true,
        minSlides: 1,
        maxSlides: 1
    });
    // muse modal_id
    $('.bxslider9').bxSlider({
        pagerCustom: '.muse-pager9',
        infiniteLoop: false,
        touchEnabled: false,
        controls: false,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider10').bxSlider({
        pagerCustom: '.muse-pager10',
        infiniteLoop: false,
        touchEnabled: false,
        controls: false,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider11').bxSlider({
        pagerCustom: '.muse-pager11',
        infiniteLoop: false,
        touchEnabled: false,
        controls: false,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider12').bxSlider({
        pagerCustom: '.muse-pager12',
        infiniteLoop: false,
        touchEnabled: false,
        controls: false,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider13').bxSlider({
        pagerCustom: '.muse-pager13',
        infiniteLoop: false,
        touchEnabled: false,
        controls: false,
        minSlides: 1,
        maxSlides: 1
    });
    $('.bxslider14').bxSlider({
        pagerCustom: '.bx-pager14',
        infiniteLoop: true,
        controls: true,
        minSlides: 1,
        maxSlides: 1
    });
});
// 一定数を超えたミュゼモーダル画像開放設定
$(function() {
  var iine = $(".count span");

  iine.each(function() {

    var iine = $(".count span",this);
    var count = parseInt($(this).text());

    if (count >= 10000 ) {
      var index = $(this).parents("li").index();
      var museModal = $("#musediv>div").eq(index);
      var target = museModal.find(".mainPhotoArea ul li:nth-child(2)");
      var targetThum = museModal.find("ul.museThum li:nth-child(2)");
      var targetImg = target.find("img").attr("src").replace("dummy", "open");
      var targetThumImg = targetThum.find("img").attr("src").replace("dummy", "open");
      var targetData = museModal.find(".museThum li:nth-child(2) a");

      target.find("img").attr("src", targetImg);
      targetThum.find("img").attr("src", targetThumImg);
      targetData.attr("data-slide-index", "1");

      if (count >= 30000 ) {
      var target = museModal.find(".mainPhotoArea ul li:nth-child(3)");
      var targetThum = museModal.find("ul.museThum li:nth-child(3)");
      var targetImg = target.find("img").attr("src").replace("dummy", "open");
      var targetThumImg = targetThum.find("img").attr("src").replace("dummy", "open");
        var targetData = museModal.find(".museThum li:nth-child(3) a");

        target.find("img").attr("src", targetImg);
        targetThum.find("img").attr("src", targetThumImg);
        targetData.attr("data-slide-index", "2");

        if (count >= 50000 ) {
        var target = museModal.find(".mainPhotoArea ul li:nth-child(4)");
        var targetThum = museModal.find("ul.museThum li:nth-child(4)");
        var targetImg = target.find("img").attr("src").replace("dummy", "open");
        var targetThumImg = targetThum.find("img").attr("src").replace("dummy", "open");
          var targetData = museModal.find(".museThum li:nth-child(4) a");

          target.find("img").attr("src", targetImg);
          targetThum.find("img").attr("src", targetThumImg);
          targetData.attr("data-slide-index", "3");

          if (count >= 70000 ) {
          var target = museModal.find(".mainPhotoArea ul li:nth-child(5)");
          var targetThum = museModal.find("ul.museThum li:nth-child(5)");
          var targetImg = target.find("img").attr("src").replace("dummy", "open");
          var targetThumImg = targetThum.find("img").attr("src").replace("dummy", "open");
            var targetData = museModal.find(".museThum li:nth-child(5) a");

            target.find("img").attr("src", targetImg);
            targetThum.find("img").attr("src", targetThumImg);
            targetData.attr("data-slide-index", "4");
          }
        }
      }
    }
  });
});

<?php
function get_count($file) {
  $filename = 'data/'.$file.'.dat';
  $fp = @fopen($filename, 'r');
  if ($fp) {
    $vote = sizeof(file($filename));
  } else {
    $vote = 0;
  }
  return $vote;
}
?>
<!doctype html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <title>MODELLISTA TOKYO AUTO SALON 2017 | トヨタモデリスタ</title>
  <meta name=”robots” content=”noindex,nofollow” />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="keywords" content="">
  <meta name="description" content="東京オートサロン2017　モデリスタ出展ブース（ルーミー、タンク）の展示車情報やイベント情報をご確認いただけます">
  <meta property="og:type" content="website">
  <meta property="fb:app_id" content="">
  <meta property="og:url" content="http://www.modellista.co.jp/event/as2017/index.html">
  <meta property="og:image" content="images/1200_630.jpg">
  <meta property="og:title" content="MODELLISTA TOKYO AUTO SALON 2017 | トヨタモデリスタ">
  <meta property="og:site_name" content="トヨタモデリスタ TOKYO AUTO SALON 2017 WEBサイト">
  <meta property="og:description" content="東京オートサロン2017　モデリスタ出展ブース（ルーミー、タンク）の展示車情報やイベント情報をご確認いただけます">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/jquery.bxslider.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery-1.8.3.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
  <script src="js/main.js"></script>
  <script src="js/smoothScroll.js"></script>
  <script src="js/jquery.bxslider.js"></script>
  <script src="js/jquery.leanModal.min.js"></script>
  <script type="text/javascript" src="/shared/js/analytics.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
</head>

<section id="event" class="event">
  <div class="inner">
    <h2><img src="images/ttl_event.png" alt="EVENT"></h2>
    <p>モデリスタブースを彩るコンパニオン「ミューズ（女神）」達が、担当車種のイチオシパーツをご紹介。<br>
      投票ページの「かわイイね！」をクリックして、みんなで応援して下さい。<br>投票数に応じて、ミューズ達の写真が増えていきます！<br>
      また投票結果は、オートサロン最終日にモデリスタブース内のファンイベントで発表され、 No.1 MUSEの授賞式を開催します。<br>
      あなたの推しミューを授賞させちゃおう！<br>
      投票はおひとり様1日1回とさせていただきます。毎日投票して応援してね！<br>
      <span class="caution">※TOP OF MUSE は、2017年1月15日（日）15：00時点での得票数にて決定させていただきます。</span></p>
      <h3><img src="images/ttl_contest_pc.png" alt="Modellista Muse Contest" class="imgChange"></h3>
      <form action="">
        <ul class="museList">
          <li>
            <a href="#museModal" rel="leanModal" data-tor-smoothScroll="noSmooth">
              <dl>
                <img src="images/ttl_contest_01_pc.png" alt="名前" class="imgChange">
              </dl>
              <img src="images/muse/lb_cnt_thum01_pc.jpg" alt="名前" class="imgChange">
            </a>
            <img id="muse_01" src="images/btn_kawaii_pc.png" alt="かわイイね！" class="kawaiiBtn imgChange">
            <p class="count"><i></i><span class="muse_01"><?= get_count('muse_01') ?></span></p>
          </li>
          <li>
            <a href="#museModal2" rel="leanModal" data-tor-smoothScroll="noSmooth">
              <dl>
                <img src="images/ttl_contest_02_pc.png" alt="名前" class="imgChange">
              </dl>
              <img src="images/muse/lb_cnt_thum02_pc.jpg" alt="名前">
            </a>
            <img id="muse_02" src="images/btn_kawaii_pc.png" alt="かわイイね！" class="kawaiiBtn imgChange">
            <p class="count"><i></i><span class="muse_02"><?= get_count('muse_02') ?></span></p>
          </li>
          <li>
            <a href="#museModal3" rel="leanModal" data-tor-smoothScroll="noSmooth">
              <dl>
                <img src="images/ttl_contest_03_pc.png" alt="名前" class="imgChange">
              </dl>
              <img src="images/muse/lb_cnt_thum03_pc.jpg" alt="名前">
            </a>
            <img id="muse_03" src="images/btn_kawaii_pc.png" alt="かわイイね！" class="kawaiiBtn imgChange">
            <p class="count"><i></i><span class="muse_03"><?= get_count('muse_03') ?></span></p>
          </li>
          <li>
            <a href="#museModal4" rel="leanModal" data-tor-smoothScroll="noSmooth">
              <dl>
                <img src="images/ttl_contest_04_pc.png" alt="名前" class="imgChange">
              </dl>
              <img src="images/muse/lb_cnt_thum04_pc.jpg" alt="名前">
            </a>
            <img id="muse_04" src="images/btn_kawaii_pc.png" alt="かわイイね！" class="kawaiiBtn imgChange">
            <p class="count"><i></i><span class="muse_04"><?= get_count('muse_04') ?></span></p>
          </li>
          <li>
            <a href="#museModal5" rel="leanModal" data-tor-smoothScroll="noSmooth">
              <dl>
                <img src="images/ttl_contest_05_pc.png" alt="名前" class="imgChange">
              </dl>
              <img src="images/muse/lb_cnt_thum05_pc.jpg" alt="名前">
            </a>
            <img id="muse_05" src="images/btn_kawaii_pc.png" alt="かわイイね！" class="kawaiiBtn imgChange">
            <p class="count"><i></i><span class="muse_05"><?= get_count('muse_05') ?></span></p>
          </li>
        </ul>
      </form>
    </div>
    <!-- muse modal event contents -->
    <div id="musediv">
      <div id="museModal" class="modalArea">
        <div class="inner">
          <div class="modalContentsBg">
            <div class="muse-pager9">
              <a class="closeBtn" data-slide-index="0">
                <span></span>
                <span></span>
              </a>
            </div>
            <div class="mainPhotoArea">
              <ul class="bxslider9">
                <li><img src="images/muse/01/lb_cnt_main01_pc.jpg" /></li>
                <li><img src="images/muse/02/lb_cnt_dummy02_main_1arisa.jpg" /></li>
                <li><img src="images/muse/03/lb_cnt_dummy03_main_1arisa.jpg" /></li>
                <li><img src="images/muse/04/lb_cnt_dummy04_main_1arisa.jpg" /></li>
                <li><img src="images/muse/05/lb_cnt_dummy05_main_1arisa.jpg" /></li>
              </ul>
            </div>
            <ul id="bxslider_muse1" class="museThum muse-pager9">
              <li>
                <a data-slide-index="0" href=""><img src="images/muse/01/lb_cnt_thum01_pc.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/02/lb_cnt_dummy02_1arisa.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/03/lb_cnt_dummy03_1arisa.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/04/lb_cnt_dummy04_1arisa.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/05/lb_cnt_dummy05_1arisa.jpg" /></a>
              </li>
            </ul>
          </div>
          <!-- .modalContentsBg -->
        </div>
        <!-- .inner -->
      </div>

      <div id="museModal2" class="modalArea">
        <div class="inner">
          <div class="modalContentsBg">
            <div class="muse-pager10">
              <a class="closeBtn" data-slide-index="0">
                <span></span>
                <span></span>
              </a>
            </div>
            <div class="mainPhotoArea">
              <ul class="bxslider10">
                <li><img src="images/muse/01/lb_cnt_main02_pc.jpg" /></li>
                <li><img src="images/muse/02/lb_cnt_dummy02_main_2eri.jpg" /></li>
                <li><img src="images/muse/03/lb_cnt_dummy03_main_2eri.jpg" /></li>
                <li><img src="images/muse/04/lb_cnt_dummy04_main_2eri.jpg" /></li>
                <li><img src="images/muse/05/lb_cnt_dummy05_main_2eri.jpg" /></li>
              </ul>
            </div>
            <ul id="bxslider_muse2" class="museThum muse-pager10">
              <li>
                <a data-slide-index="0" href=""><img src="images/muse/01/lb_cnt_thum02_pc.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/02/lb_cnt_dummy02_2eri.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/03/lb_cnt_dummy03_2eri.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/04/lb_cnt_dummy04_2eri.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/05/lb_cnt_dummy05_2eri.jpg" /></a>
              </li>
            </ul>
          </div>
          <!-- .modalContentsBg -->
        </div>
        <!-- .inner -->
      </div>

      <div id="museModal3" class="modalArea">
        <div class="inner">
          <div class="modalContentsBg">
            <div class="muse-pager11">
              <a class="closeBtn" data-slide-index="0">
                <span></span>
                <span></span>
              </a>
            </div>
            <div class="mainPhotoArea">
              <ul class="bxslider11">
                <li><img src="images/muse/01/lb_cnt_main03_pc.jpg" /></li>
                <li><img src="images/muse/02/lb_cnt_dummy02_main_3marie.jpg" /></li>
                <li><img src="images/muse/03/lb_cnt_dummy03_main_3marie.jpg" /></li>
                <li><img src="images/muse/04/lb_cnt_dummy04_main_3marie.jpg" /></li>
                <li><img src="images/muse/05/lb_cnt_dummy05_main_3marie.jpg" /></li>
              </ul>
            </div>
            <ul id="bxslider_muse3" class="museThum muse-pager11">
              <li>
                <a data-slide-index="0" href=""><img src="images/muse/01/lb_cnt_thum03_pc.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/02/lb_cnt_dummy02_3marie.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/03/lb_cnt_dummy03_3marie.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/04/lb_cnt_dummy04_3marie.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/05/lb_cnt_dummy05_3marie.jpg" /></a>
              </li>
            </ul>
          </div>
          <!-- .modalContentsBg -->
        </div>
        <!-- .inner -->
      </div>

      <div id="museModal4" class="modalArea">
        <div class="inner">
          <div class="modalContentsBg">
            <div class="muse-pager12">
              <a class="closeBtn" data-slide-index="0">
                <span></span>
                <span></span>
              </a>
            </div>
            <div class="mainPhotoArea">
              <ul class="bxslider12">
                <li><img src="images/muse/01/lb_cnt_main04_pc.jpg" /></li>
                <li><img src="images/muse/02/lb_cnt_dummy02_main_4chiho.jpg" /></li>
                <li><img src="images/muse/03/lb_cnt_dummy03_main_4chiho.jpg" /></li>
                <li><img src="images/muse/04/lb_cnt_dummy04_main_4chiho.jpg" /></li>
                <li><img src="images/muse/05/lb_cnt_dummy05_main_4chiho.jpg" /></li>
              </ul>
            </div>
            <ul id="bxslider_muse4" class="museThum muse-pager12">
              <li>
                <a data-slide-index="0" href=""><img src="images/muse/01/lb_cnt_thum04_pc.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/02/lb_cnt_dummy02_4chiho.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/03/lb_cnt_dummy03_4chiho.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/04/lb_cnt_dummy04_4chiho.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/05/lb_cnt_dummy05_4chiho.jpg" /></a>
              </li>
            </ul>
          </div>
          <!-- .modalContentsBg -->
        </div>
        <!-- .inner -->
      </div>

      <div id="museModal5" class="modalArea">
        <div class="inner">
          <div class="modalContentsBg">
            <div class="muse-pager13">
              <a class="closeBtn" data-slide-index="0">
                <span></span>
                <span></span>
              </a>
            </div>
            <div class="mainPhotoArea">
              <ul class="bxslider13">
                <li><img src="images/muse/01/lb_cnt_main05_pc.jpg" /></li>
                <li><img src="images/muse/02/lb_cnt_dummy02_main_5ran.jpg" /></li>
                <li><img src="images/muse/03/lb_cnt_dummy03_main_5ran.jpg" /></li>
                <li><img src="images/muse/04/lb_cnt_dummy04_main_5ran.jpg" /></li>
                <li><img src="images/muse/05/lb_cnt_dummy05_main_5ran.jpg" /></li>
              </ul>
            </div>
            <ul id="bxslider_muse5" class="museThum muse-pager13">
              <li>
                <a data-slide-index="0" href=""><img src="images/muse/01/lb_cnt_thum05_pc.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/02/lb_cnt_dummy02_5ran.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/03/lb_cnt_dummy03_5ran.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/04/lb_cnt_dummy04_5ran.jpg" /></a>
              </li>
              <li>
                <a data-slide-index="" href=""><img src="images/muse/05/lb_cnt_dummy05_5ran.jpg" /></a>
              </li>
            </ul>
          </div>
          <!-- .modalContentsBg -->
        </div>
        <!-- .inner -->
      </div>
    </div>
  </section>
  <!-- /.event -->
  <script>
$(function() {
    var wid = $(window).width();
    if (wid < 768) {
        $('.imgChange').each(function() {
            $(this).attr("src", $(this).attr("src").replace('_pc', '_sp'));
        });
    }
});
$('.kawaiiBtn').click(function() {
    if ($.cookie('clickBtn') != 'on') {
    var date = new Date();
    var midnight = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);
    $(this).next().find("i").toggleClass('click');
    var id = $(this).attr('id');
    Vote(id);
    var countTxt = parseInt($(this).next().find("span").text());
    var number = countTxt + 1;

    $(this).next().find("span").text(number);
    $.cookie('clickBtn', 'on', {
      expires: midnight,
      path:'/'
    });
  }
});
function Vote(id) {
  cls = $('.' + id);
  cls_num = Number(cls.html());
  count =  cls_num + 1 + "\n";
  $.post('vote.php', {
    'file': id,
    'count': count
  });
}
$(window).on("load ready", function() {
  var imgHeight = $(".mainPhotoArea li:first-child img").outerHeight();
  $(".mainPhotoArea").css("height", imgHeight);
});
  </script>
  <script src="https://genieedmp.com/dmp.js?c=1024"></script>
  </body>

  </html>
